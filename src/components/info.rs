use crate::components::info_branch::InfoBranchModel;
use crate::components::info_default::InfoDefaultModel;
use crate::components::info_project::InfoProjectModel;
use crate::components::messages::MessagesModel;
use crate::event_bus::{EventBus, Request};
use crate::types::app_state::AppState;
use crate::types::general::HeaderMessage;
use crate::types::general::{CommitsListResult, PipelinesListResult, ViewSelected};
use yew::agent::{Dispatched, Dispatcher};
use yew::prelude::*;
use yew::services::fetch::FetchTask;
use yew::services::Task;

#[derive(PartialEq, Properties, Clone, Debug)]
pub struct Props {
    pub app_state: AppState,
    pub selected_view: ViewSelected,
    pub on_app_state_updated: Callback<AppState>,
    pub on_view_selected: Callback<ViewSelected>,
}

pub enum Msg {
    FetchProjectPipelinePage(u16, u32),
    ProjectsFetchPipelinesReady(PipelinesListResult),
    FetchProjectCommitsPage(u16, u32),
    ProjectFetchCommitsReady(CommitsListResult),
    ViewChanged(ViewSelected),
    UpdateProject(u32),
    FetchAllProjectsData,
}

pub struct InfoModel {
    props: Props,
    link: ComponentLink<InfoModel>,
    project_pipelines_callback: Callback<PipelinesListResult>,
    project_commits_callback: Callback<CommitsListResult>,
    fetch_tasks: Vec<Option<FetchTask>>,
    pipeline_pages_to_fetch: u16,
    commits_pages_to_fetch: u16,
    event_bus: Dispatcher<EventBus>,
}

impl Component for InfoModel {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let event_bus = EventBus::dispatcher();
        InfoModel {
            props,
            link: link.clone(),
            fetch_tasks: Vec::new(),
            project_pipelines_callback: link.callback(Msg::ProjectsFetchPipelinesReady),
            project_commits_callback: link.callback(Msg::ProjectFetchCommitsReady),
            pipeline_pages_to_fetch: 2,
            commits_pages_to_fetch: 4,
            event_bus,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::FetchAllProjectsData => {
                self.schedule_all_pipelines_fetches();
                self.schedule_all_commits_fetches();
                false
            }

            Msg::FetchProjectPipelinePage(page_num, project_id) => {
                let api_key = self.props.app_state.creds.api_key.clone();
                if let Some(project_name) =
                    self.props.app_state.get_project_name_from_id(project_id)
                {
                    self.add_display_message(
                        format!(
                            "Fetching pipelines for project {}, page {}",
                            project_name, page_num
                        )
                        .as_str(),
                    );
                }
                let project = self.props.app_state.projects.get(&project_id).unwrap();
                let fetch_task = project.pipelines_fetch_task(
                    page_num,
                    self.project_pipelines_callback.clone(),
                    api_key,
                );
                self.fetch_tasks.push(Some(fetch_task));
                true
            }

            Msg::ProjectsFetchPipelinesReady(pipelines_request_result) => {
                match pipelines_request_result.pipelines_result {
                    Ok(pipelines_list) => {
                        if let Some(project_name) = self
                            .props
                            .app_state
                            .get_project_name_from_id(pipelines_request_result.project_id)
                        {
                            self.remove_display_message(
                                format!(
                                    "Fetching pipelines for project {}, page {}",
                                    project_name, pipelines_request_result.page_num
                                )
                                .as_str(),
                            );
                        }

                        if let Some(project) = self
                            .props
                            .app_state
                            .projects
                            .get_mut(&pipelines_request_result.project_id)
                        {
                            for pipeline in pipelines_list.iter() {
                                project.pipelines.insert(pipeline.id, pipeline.clone());
                            }
                            self.props.app_state.save_state();
                            self.props
                                .on_app_state_updated
                                .emit(self.props.app_state.clone());
                        }
                    }
                    Err(e) => {
                        info!("Error getting pipelines {:?}", e);
                    }
                }
                self.remove_completed_tasks();
                true
            }

            Msg::FetchProjectCommitsPage(page_num, project_id) => {
                let api_key = self.props.app_state.creds.api_key.clone();
                if let Some(project_name) =
                    self.props.app_state.get_project_name_from_id(project_id)
                {
                    self.add_display_message(
                        format!(
                            "Fetching commits for project {}, page {}",
                            project_name, page_num
                        )
                        .as_str(),
                    );
                }
                let project = self.props.app_state.projects.get(&project_id).unwrap();

                let fetch_task = project.commits_fetch_task(
                    page_num,
                    self.project_commits_callback.clone(),
                    api_key,
                );

                self.fetch_tasks.push(Some(fetch_task));
                true
            }

            Msg::ProjectFetchCommitsReady(commits_request_result) => {
                match commits_request_result.commits_result {
                    Ok(commits_list) => {
                        if let Some(project_name) = self
                            .props
                            .app_state
                            .get_project_name_from_id(commits_request_result.project_id)
                        {
                            self.remove_display_message(
                                format!(
                                    "Fetching commits for project {}, page {}",
                                    project_name, commits_request_result.page_num
                                )
                                .as_str(),
                            );
                        }

                        if let Some(project) = self
                            .props
                            .app_state
                            .projects
                            .get_mut(&commits_request_result.project_id)
                        {
                            for commit in commits_list.iter() {
                                project.commits.insert(commit.id.clone(), commit.clone());
                            }
                            self.props.app_state.save_state();
                            self.props
                                .on_app_state_updated
                                .emit(self.props.app_state.clone());
                        }
                    }
                    Err(e) => {
                        info!("Error getting pipelines {:?}", e);
                    }
                }
                self.remove_completed_tasks();
                true
            }

            Msg::ViewChanged(view_selected) => {
                self.props.on_view_selected.emit(view_selected);
                false
            }

            Msg::UpdateProject(project_id) => {
                self.schedule_project_pipelines_fetches(project_id);
                self.schedule_project_commits_fetches(project_id);
                false
            }
        }
    }

    fn view(&self) -> Html {
        let view_page = || -> Html {
            let view = match &self.props.selected_view {
                ViewSelected::Default => {
                    html! { <InfoDefaultModel projects=&self.props.app_state.projects /> }
                }

                ViewSelected::Project(project) => {
                    html! { <InfoProjectModel project=project on_view_selected=self.link.callback(|view_selected| Msg::ViewChanged(view_selected)) on_update_project=self.link.callback(|project_id| Msg::UpdateProject(project_id)) /> }
                }

                ViewSelected::Branch(project, branch_name) => {
                    html! { <InfoBranchModel project=project branch_name=branch_name  /> }
                }
            };
            view
        };
        html! {
            <>
                <div class="content-wrapper" style="min-height: 1200.88px;">
                    <MessagesModel />
                    { view_page()}
                </div>
            </>
        }
    }

    fn change(&mut self, new_props: Self::Properties) -> ShouldRender {
        if self.props.app_state.projects != new_props.app_state.projects {
            self.props.app_state = new_props.app_state;
            self.link.send_message(Msg::FetchAllProjectsData);
        }
        self.props.selected_view = new_props.selected_view;
        true
    }
}

impl InfoModel {
    fn schedule_project_pipelines_fetches(&mut self, project_id: u32) {
        let mut messages: Vec<Msg> = Vec::new();
        for i in 1..=self.pipeline_pages_to_fetch {
            messages.push(Msg::FetchProjectPipelinePage(i, project_id));
        }
        self.link.send_message_batch(messages);
    }

    fn schedule_all_pipelines_fetches(&mut self) {
        for project_id in self.props.app_state.get_projects_ids() {
            self.schedule_project_pipelines_fetches(project_id);
        }
    }

    fn schedule_project_commits_fetches(&mut self, project_id: u32) {
        let mut messages: Vec<Msg> = Vec::new();
        for i in 1..=self.commits_pages_to_fetch {
            messages.push(Msg::FetchProjectCommitsPage(i, project_id));
        }
        self.link.send_message_batch(messages);
    }

    fn schedule_all_commits_fetches(&mut self) {
        for project_id in self.props.app_state.get_projects_ids() {
            self.schedule_project_commits_fetches(project_id);
        }
    }

    fn remove_completed_tasks(&mut self) {
        self.fetch_tasks.retain(|t| t.as_ref().is_some());
        self.fetch_tasks.retain(|t| t.as_ref().unwrap().is_active());
    }

    fn add_display_message(&mut self, message: &str) {
        let msg: HeaderMessage = HeaderMessage::Add(String::from(message));
        self.event_bus.send(Request::EventBusMsg(msg));
    }

    fn remove_display_message(&mut self, message: &str) {
        let msg: HeaderMessage = HeaderMessage::Remove(String::from(message));
        self.event_bus.send(Request::EventBusMsg(msg));
    }
}
