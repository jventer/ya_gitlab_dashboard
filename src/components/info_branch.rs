use crate::types::general::Pipeline;
use crate::types::project::Project;
use std::collections::BTreeMap;
use yew::prelude::*;

#[derive(PartialEq, Properties, Clone, Debug)]
pub struct Props {
    pub project: Project,
    pub branch_name: String,
}

pub enum Msg {}

#[derive(Debug)]
pub struct InfoBranchModel {
    props: Props,
    link: ComponentLink<InfoBranchModel>,
    branch_pipelines: Vec<Pipeline>,
    grouped_branch_pipelines: BTreeMap<String, Vec<Pipeline>>,
}

impl Component for InfoBranchModel {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let mut model = InfoBranchModel {
            props,
            link,
            branch_pipelines: Vec::new(),
            grouped_branch_pipelines: BTreeMap::new(),
        };
        model.branch_pipelines = model
            .props
            .project
            .get_pipelines_per_branch(&model.props.branch_name);

        for pipeline in model.branch_pipelines.iter() {
            let mut key = pipeline.updated_at.clone();
            key.truncate(10);
            model
                .grouped_branch_pipelines
                .entry(key)
                .or_insert(Vec::new())
                .push(pipeline.clone());
        }
        model
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        // let mut dates: Vec<String> = self.grouped_branch_pipelines.keys()
        let mut dates: Vec<String> = Vec::new();
        for key in self.grouped_branch_pipelines.keys() {
            dates.push(key.clone());
        }
        dates.reverse();

        html! {

          <>

              <section class="content-header">
                <div class="container-fluid">
                  <div class="row mb-2">
                    <div class="col-sm-6">
                      <h1>{format!("Pipelines: {}", self.props.branch_name)}</h1>
                    </div>
                  </div>
                </div>
              </section>



        <section class="content">
          <div class="container-fluid">

            <div class="row">
              <div class="col-md-12">
                <div class="timeline">

                  // { self.branch_pipelines.iter().map(|pipeline| self.view_pipeline(pipeline.clone())).collect::<Html>() }
                  { dates.iter().map(|date| self.view_pipeline_day(date.clone())).collect::<Html>() }

                  <div>
                    <i class="fas fa-clock bg-gray"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </section>
        </>
            // </div>
            }
    }

    fn change(&mut self, new_props: Self::Properties) -> ShouldRender {
        self.props = new_props;
        true
    }
}

impl InfoBranchModel {
    fn view_pipeline_day(&self, date: String) -> Html {
        let date_pipelines = self.grouped_branch_pipelines.get(&date).unwrap();
        html! {
            <>
            <div class="time-label">
                <span class="bg-blue">{ date.clone() }</span>
            </div>
              { date_pipelines.iter().map(|pipeline| self.view_pipeline(pipeline.clone())).collect::<Html>() }
            </>
        }
    }

    fn view_pipeline(&self, pipeline: Pipeline) -> Html {
        let show_status_span = |status| -> Html {
            match status {
                "success" => return html! { <i class="fas fa-edit bg-green"></i> },

                "failed" => return html! { <i class="fas fa-edit bg-red"></i> },

                _ => return html! { <i class="fas fa-edit bg-warning"></i> },
            }
        };

        match self.props.project.commits.get(&pipeline.sha) {
            Some(commit) => {
                html! {
                      <div>
                        { show_status_span( &pipeline.status  ) }
                        <div class="timeline-item">
                          <span class="time"><i class="fa fa-clock-o"></i>{ commit.committed_date.clone() }</span>
                          <h3 class="timeline-header no-border">{ commit.title.clone() }</h3>
                          <div class="timeline-body">
                            <p>{ commit.message.clone() }</p>
                            <p>{ format!("Commit by {} ({})",commit.committer_name, commit.committer_email) }</p>
                            <p>
                              <a target={ "_blank" } class="link" href={  pipeline.web_url.clone() }><i class="pl-3 fab fa-gitlab"></i><span class="pl-2 brand-text font-weight-light">{ "Pipeline" }</span></a>
                              <a target={ "_blank" } class="link" href={ commit.web_url.clone() }><i class="pl-3 fab fa-gitlab"></i><span class="pl-2 brand-text font-weight-light">{ "Commit" }</span></a>
                            </p>
                          </div>
                        </div>
                      </div>
                }
            }
            None => {
                let mut commit_url_list: Vec<&str> =
                    pipeline.web_url.split("/pipelines/").collect();
                commit_url_list.truncate(1);
                commit_url_list.push("-/commit/");
                commit_url_list.push(pipeline.sha.as_str());
                let commit_url = commit_url_list.join("/");

                html! {
                          <div>
                            { show_status_span( &pipeline.status  ) }
                            <div class="timeline-item">
                              <h3 class="timeline-header no-border">{ pipeline.updated_at.clone() }</h3>
                              <a target={ "_blank" } class="link" href={  pipeline.web_url.clone() }><i class="pl-1 fab fa-gitlab"></i><span class="pl-2 brand-text font-weight-light">{ "Pipeline" }</span></a>
                              <a target={ "_blank" } class="link" href={ commit_url }><i class="pl-1 fab fa-gitlab"></i><span class="pl-2 brand-text font-weight-light">{ "Commit" }</span></a>
                            </div>
                          </div>
                }
            }
        }
    }
}
