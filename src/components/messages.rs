use crate::types::general::HeaderMessage;
use yew::prelude::*;
use yew::{html, Bridge, Component, ComponentLink, Html, ShouldRender};

use crate::event_bus::EventBus;
use yew::agent::Bridged;
// use yew::{html, Bridge, Component, ComponentLink, Html, ShouldRender};

#[derive(PartialEq, Properties, Clone, Debug)]
pub struct Props {}

pub struct MessagesModel {
    props: Props,
    messages: Vec<String>,
    _producer: Box<dyn Bridge<EventBus>>,
}

pub enum Msg {
    NewMessage(String),
}

impl Component for MessagesModel {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let callback = link.callback(Msg::NewMessage);
        let _producer = EventBus::bridge(callback);
        MessagesModel {
            props,
            messages: Vec::default(),
            _producer,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::NewMessage(s) => {
                let header_message: HeaderMessage = serde_json::from_str(s.as_str()).unwrap();
                match header_message {
                    HeaderMessage::Add(message) => {
                        self.messages.push(message);
                    }
                    HeaderMessage::Remove(message) => {
                        self.messages.retain(|x| *x != message);
                    }
                }
            }
        }
        true
    }

    fn view(&self) -> Html {
        let display_message = |message: &String| -> Html {
            return html! {
                <> { message.clone() } <br/></>
            };
        };

        if !self.messages.is_empty() {
            html! {
                <section class="content">
                    <div class="alert alert-info" role="alert">
                        { self.messages.iter().map(|message| display_message(message)).collect::<Html>() }
                    </div>
                </section>
            }
        } else {
            html! {}
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props = props;
        true
    }
}
