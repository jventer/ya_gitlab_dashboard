use yew::prelude::*;
use yew::{html, Component, ComponentLink, Html, ShouldRender};

#[derive(PartialEq, Properties, Clone, Debug)]
pub struct Props {}

pub struct Footer {
    pub link: ComponentLink<Footer>,
    pub props: Props,
}

pub enum Msg {}

impl Component for Footer {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Footer { props, link }
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        html! {
            <footer class="main-footer">
                <div class="float-right d-none d-sm-inline">
                    {"YAGD"}
                </div>
            </footer>
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props = props;
        true
    }
}
