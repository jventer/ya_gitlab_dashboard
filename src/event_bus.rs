// Adapted from https://github.com/yewstack/yew/blob/master/examples/pub_sub/src/event_bus.rs

use crate::types::general::HeaderMessage;
use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use yew::worker::*;

use serde_json;

#[derive(Serialize, Deserialize, Debug)]
pub enum Request {
    EventBusMsg(HeaderMessage),
}

pub struct EventBus {
    link: AgentLink<EventBus>,
    subscribers: HashSet<HandlerId>,
}

impl Agent for EventBus {
    type Reach = Context;
    type Message = ();
    type Input = Request;
    type Output = String;

    fn create(link: AgentLink<Self>) -> Self {
        EventBus {
            link,
            subscribers: HashSet::new(),
        }
    }

    fn update(&mut self, _: Self::Message) {}

    fn handle_input(&mut self, msg: Self::Input, _: HandlerId) {
        match msg {
            Request::EventBusMsg(s) => {
                for sub in self.subscribers.iter() {
                    self.link.respond(*sub, serde_json::to_string(&s).unwrap());
                }
            }
        }
    }

    fn connected(&mut self, id: HandlerId) {
        self.subscribers.insert(id);
    }

    fn disconnected(&mut self, id: HandlerId) {
        self.subscribers.remove(&id);
    }
}
