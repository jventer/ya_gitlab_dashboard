use crate::types::project::Project;
use anyhow::Error;
use http::response::Parts;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum HeaderMessage {
    Add(String),
    Remove(String),
}

#[derive(Default, Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct Creds {
    pub api_key: String,
    pub project_names: String,
    pub group_name: String,
    pub projects_requested: Vec<String>,
}

#[derive(Default, Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct FetchingStatusMessage {
    pub message: String,
}

#[derive(Debug)]
pub struct ProjectsListResult {
    pub projects_result: Result<Vec<Project>, Error>,
    pub meta: Parts,
    pub page_num: String,
}

#[derive(Debug)]
pub struct BranchesListResult {
    pub branches_result: Result<Vec<Branch>, Error>,
    pub meta: Parts,
    pub project_id: u32,
}

#[derive(Debug)]
pub struct PipelinesListResult {
    pub pipelines_result: Result<Vec<Pipeline>, Error>,
    pub meta: Parts,
    pub project_id: u32,
    pub page_num: String,
}

#[derive(Debug)]
pub struct CommitsListResult {
    pub commits_result: Result<Vec<Commit>, Error>,
    pub meta: Parts,
    pub project_id: u32,
    pub page_num: String,
}

// #[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
// pub struct Group {
//     pub id: i64,
//     pub web_url: String,
//     pub name: String,
//     pub path: String,
//     pub description: String,
//     // pub visibility: String,
//     // pub share_with_group_lock: bool,
//     // pub require_two_factor_authentication: bool,
//     // pub two_factor_grace_period: i64,
//     // pub project_creation_level: String,
//     // pub auto_devops_enabled: ::serde_json::Value,
//     // pub subgroup_creation_level: String,
//     // pub emails_disabled: bool,
//     // pub mentions_disabled: ::serde_json::Value,
//     // pub lfs_enabled: bool,
//     // pub default_branch_protection: i64,
//     // pub avatar_url: String,
//     // pub request_access_enabled: bool,
//     // pub full_name: String,
//     // pub full_path: String,
//     // pub created_at: String,
//     // pub parent_id: ::serde_json::Value,
//     // pub ldap_cn: ::serde_json::Value,
//     // pub ldap_access: ::serde_json::Value,
//     // pub file_template_project_id: i64,
//     // pub marked_for_deletion_on: ::serde_json::Value,
// }

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct Namespace {
    pub id: i64,
    pub name: String,
    pub path: String,
    pub kind: String,
    pub full_path: String,
    pub parent_id: Option<i64>,
    pub avatar_url: String,
    pub web_url: String,
}

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct Links {
    #[serde(rename = "self")]
    pub self_field: String,
    pub issues: Option<String>,
    pub merge_requests: Option<String>,
    pub repo_branches: String,
    pub labels: String,
    pub events: String,
    pub members: String,
}

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct ContainerExpirationPolicy {
    pub cadence: String,
    pub enabled: bool,
    pub keep_n: Option<String>,
    pub older_than: Option<String>,
    pub name_regex: Option<String>,
    pub next_run_at: Option<String>,
}

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct SharedWithGroup {
    pub group_id: i64,
    pub group_name: String,
    pub group_full_path: String,
    pub group_access_level: i64,
    pub expires_at: Option<String>,
}

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct ForkedFromProject {
    pub id: i64,
    pub description: String,
    pub name: String,
    pub name_with_namespace: String,
    pub path: String,
    pub path_with_namespace: String,
    pub created_at: String,
    pub default_branch: String,
    pub tag_list: Vec<String>,
    pub ssh_url_to_repo: String,
    pub http_url_to_repo: String,
    pub web_url: String,
    pub readme_url: String,
    pub avatar_url: String,
    pub star_count: i64,
    pub forks_count: i64,
    pub last_activity_at: String,
    pub namespace: Namespace,
}

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct Branch {
    pub name: String,
    pub commit: Commit,
    // pub merged: bool,
    // pub protected: bool,
    // pub developers_can_push: bool,
    // pub developers_can_merge: bool,
    // pub can_push: bool,
    // pub default: bool,
    pub web_url: String,
}

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct Commit {
    pub id: String,
    pub short_id: String,
    pub created_at: String,
    pub parent_ids: ::serde_json::Value,
    pub title: String,
    pub message: String,
    pub author_name: String,
    pub author_email: String,
    pub authored_date: String,
    pub committer_name: String,
    pub committer_email: String,
    pub committed_date: String,
    pub web_url: String,
}

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct Pipeline {
    pub id: i64,
    pub status: String,
    #[serde(rename = "ref")]
    pub ref_field: String,
    pub sha: String,
    pub web_url: String,
    pub created_at: String,
    pub updated_at: String,
}

#[derive(PartialEq, Clone, Debug)]
pub enum ViewSelected {
    Default,
    Project(Project),
    Branch(Project, String),
}
