use anyhow::{anyhow, Error};
use yew::callback::Callback;
use yew::format::{Json, Nothing};
use yew::services::fetch::{FetchService, FetchTask, Request, Response};

use crate::utils::API_URL_PREFIX;
#[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct Group {
    pub id: i64,
    pub web_url: String,
    pub name: String,
    pub path: String,
    pub description: String,
    // pub visibility: String,
    // pub share_with_group_lock: bool,
    // pub require_two_factor_authentication: bool,
    // pub two_factor_grace_period: i64,
    // pub project_creation_level: String,
    // pub auto_devops_enabled: ::serde_json::Value,
    // pub subgroup_creation_level: String,
    // pub emails_disabled: bool,
    // pub mentions_disabled: ::serde_json::Value,
    // pub lfs_enabled: bool,
    // pub default_branch_protection: i64,
    // pub avatar_url: String,
    // pub request_access_enabled: bool,
    // pub full_name: String,
    // pub full_path: String,
    // pub created_at: String,
    // pub parent_id: ::serde_json::Value,
    // pub ldap_cn: ::serde_json::Value,
    // pub ldap_access: ::serde_json::Value,
    // pub file_template_project_id: i64,
    // pub marked_for_deletion_on: ::serde_json::Value,
}

pub fn group_fetch_task(
    group_name: String,
    callback: Callback<Result<Group, Error>>,
    api_key: String,
) -> FetchTask {
    let mut web = FetchService::new();
    let url = format!(
        "{}groups/{}?with_projects=false",
        API_URL_PREFIX, group_name
    );
    let handler = move |response: Response<Json<Result<Group, Error>>>| {
        let (meta, Json(data)) = response.into_parts();
        if meta.status.is_success() {
            callback.emit(data)
        } else {
            callback.emit(Err(anyhow!(meta.status)))
        }
    };

    let mut request = Request::builder();
    request = request
        .uri(url.as_str())
        .header("Accept", "application/json");
    if !api_key.is_empty() {
        request = request.header("PRIVATE-TOKEN", api_key);
    }
    web.fetch(request.body(Nothing).unwrap(), handler.into())
        .unwrap()
}
