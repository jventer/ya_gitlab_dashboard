use crate::types::general::Creds;
use crate::types::group::Group;
use crate::types::project::Project;
use crate::utils::get_cred_settings;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use yew::format::Json;
use yew::services::storage::{Area, StorageService};

const KEY: &str = "yew.gitlab.dashboard";

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct AppState {
    pub creds: Creds,
    pub group: Group,
    pub projects: BTreeMap<u32, Project>,
}

impl AppState {
    fn get_storage() -> Option<StorageService> {
        if let Ok(storage_service) = StorageService::new(Area::Local) {
            Some(storage_service)
        } else {
            None
        }
    }

    pub fn save_state(&self) {
        if let Some(mut store) = AppState::get_storage() {
            store.store(KEY, Json(&self));
        }
    }

    pub fn delete_state(&self) {
        if let Some(mut store) = AppState::get_storage() {
            store.remove(KEY);
            store.store(KEY, Json(&AppState::default()));
        }
    }

    fn get_locally_saved_state() -> Option<AppState> {
        if let Some(storage_service) = AppState::get_storage() {
            if let Some(found_value) = storage_service.restore(KEY) {
                if let Ok(stored_string) = found_value {
                    let res = serde_json::from_str(stored_string.as_str());
                    match res {
                        Ok(state) => {
                            return Some(state);
                        }
                        Err(state) => {
                            info!("Could not parse JSON: {:?}", state);
                            return None;
                        }
                    };
                }
            }
        }
        None
    }

    pub fn get_projects_ids(&self) -> Vec<u32> {
        let mut ids: Vec<u32> = Vec::default();
        for id in self.projects.keys() {
            ids.push(id.clone());
        }
        ids
    }

    pub fn get_project_name_from_id(&self, project_id: u32) -> Option<String> {
        if let Some(project) = self.projects.get(&project_id) {
            Some(project.name.clone())
        } else {
            None
        }
    }
}

impl Default for AppState {
    fn default() -> AppState {
        if let Some(saved_app_state) = AppState::get_locally_saved_state() {
            saved_app_state
        } else {
            let creds: Creds = get_cred_settings();
            AppState {
                creds,
                group: Group::default(),
                projects: BTreeMap::default(),
            }
        }
    }
}
