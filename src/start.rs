use crate::types::general::Creds;
use web_sys::Element;
use yew::prelude::*;

#[derive(PartialEq, Properties, Clone)]
pub struct Props {
    /// Callback when user is logged in successfully
    pub on_start_callback: Callback<Creds>,
}

pub enum Msg {
    ContinueStart,
    UpdateProjectName(String),
    UpdateApiKey(String),
    UpdateGroupName(String),
}

pub struct StartModel {
    props: Props,
    link: ComponentLink<Self>,
    disable_continue: bool,
    creds: Creds,
}

impl Component for StartModel {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        StartModel {
            props,
            link,
            creds: Creds::default(),
            disable_continue: true,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::UpdateProjectName(value) => {
                self.creds.projects_requested =
                    value.as_str().split(',').map(|s| s.to_string()).collect();
                self.creds.project_names = value;
            }
            Msg::UpdateGroupName(value) => match value.as_str() {
                "" => {
                    if self.creds.project_names.is_empty() {
                        self.disable_continue = true;
                    } else {
                        self.disable_continue = false;
                    }
                }
                _ => {
                    self.creds.group_name = value;
                    self.disable_continue = false;
                }
            },
            Msg::UpdateApiKey(value) => match value.as_str() {
                "" => {
                    self.creds.api_key = String::from("");
                }
                _ => self.creds.api_key = value,
            },
            Msg::ContinueStart => self.props.on_start_callback.emit(self.creds.clone()),
        }
        true
    }

    fn view(&self) -> Html {
        let oninput_project = self
            .link
            .callback(|ev: InputData| Msg::UpdateProjectName(ev.value));
        let oninput_group_name = self
            .link
            .callback(|ev: InputData| Msg::UpdateGroupName(ev.value));
        let oninput_api_key = self
            .link
            .callback(|ev: InputData| Msg::UpdateApiKey(ev.value));
        let on_start_click = self.link.callback(|ev: Event| {
            ev.prevent_default(); /* Prevent event propagation */
            Msg::ContinueStart
        });

        let document = yew::utils::document();
        let body: Element = document.query_selector("body").unwrap().unwrap();
        let body_class_list = body.class_list();
        body_class_list
            .add_2("hold-transition", "register-page")
            .unwrap();

        html! {

          <div class="register-box">
            <div class="register-logo">
              <a href="#">{"Yet Another Gitlab Dashboard"}</a>
            </div>

            <div class="register-box-body">
              <p class="login-box-msg">{"A Simple Single Page Gitlab Dashboard"}</p>

              <form role="form" onsubmit=on_start_click>

                <div class="form-group">
                <label for="projects">{"Projects"}</label>
                <input oninput=&oninput_project id="projects" type="text" placeholder="Comma seperated list of projects" class="form-control" value=""/>
                <span class="help-block">{"If this field is left open all projects in the group will be retrieved"}</span>
                </div>

                <div class="form-group">
                <label for="group">{"Group Name"}</label>
                <input oninput=&oninput_group_name id="group" type="text" placeholder="" class="form-control" required=true value=""/>
                <span class="help-block">{"Required"}</span>
                </div>

                <div class="form-group">
                <label for="api_key">{"API Key"}</label>
                <input oninput=&oninput_api_key id="api_key" type="text" placeholder="" class="form-control" value=""/>
                <span class="help-block">{"Required if the group/project is private"}</span>
                </div>
                  <div class="box-footer">
                    <button disabled={ self.disable_continue } type="submit" class="btn btn-primary btn-flat">
                      {"Start"}
                    </button>
                  </div>
              </form>
            </div>
            <br/>
            <p><b><u>{"Usage:"}</u></b></p>
            <p>{"Multiple projects within the same group can be displayed."}<br/>
               {"Only one group can be used."}<br/><br/>
               {"E.g. to display the following projects:"}
            </p>
             <p>
                <ul>
                  <li>{"https://gitlab.com/gitlab-org/terraform-images"}</li>
                  <li>{"https://gitlab.com/gitlab-org/slack-notifier"}</li>
                </ul>
             </p>
             <p>{"Use these settings:"}</p>
             <ul>
             <li>{"Projects"}
                <ul>
                    <li>{"terraform-images,slack-notifier"}</li>
                </ul>
             </li>
             <li>{"Group Name"}
                <ul>
                    <li>{"gitlab-org"}</li>
                </ul>
             </li>
             </ul>
          </div>
        }
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        true
    }
}
