"""
Serve the current directory
"""
import socketserver


from http.server import SimpleHTTPRequestHandler

PORT = 8089


class MyHandler(SimpleHTTPRequestHandler):
    def send_error(self, code, message=None, explain=None):
        # See https://github.com/yewstack/yew_router#server-configuration
        if code == 404:
            if not self.path.endswith(tuple(self.extensions_map.keys())[:-1]):
                self.path = "/"
                return self.do_GET()
        return SimpleHTTPRequestHandler.send_error(self, code, message)


MyHandler.extensions_map = {
    ".manifest": "text/cache-manifest",
    ".html": "text/html",
    ".png": "image/png",
    ".jpg": "image/jpg",
    ".svg": "image/svg+xml",
    ".wasm": "application/wasm",
    ".css": "text/css",
    ".ico": "image/x-icon",
    ".js": "application/x-javascript",
    "": "application/octet-stream",  # Default
}

httpd = socketserver.TCPServer(("", PORT), MyHandler)

print("serving at port", PORT)
httpd.serve_forever()
